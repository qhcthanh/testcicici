//
//  AppDelegate.h
//  testcicici
//
//  Created by thanhqhc on 4/4/17.
//  Copyright © 2017 VNG Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

